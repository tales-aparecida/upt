"""Restraint Watcher file."""

from pathlib import Path
from time import sleep
import typing
import xml.etree.ElementTree as ET

from .job_element import RestraintJob
from .restraint_dataclasses import RestraintJobDiff
from .restraint_dataclasses import RestraintRecipeDiff


class RestraintParsedError(Exception):
    """Raised when we're not able to parse a restraint object."""


class RestraintJobWatcher:
    """Class to watch restraint jobs."""

    def __init__(self, path: typing.Union[str, Path]) -> None:
        """Create the object."""
        self.restraint_delays: typing.Sequence[float] = [0.] + [.1] * 3 + [1] * 3 + [3] * 3
        self.path: Path = Path(path)
        self.job: RestraintJob = self.get_restraint_info()
        self.new_job: typing.Optional[RestraintJob] = None

    def get_restraint_info(self) -> RestraintJob:
        """
        Update file and return the restraint job file.

        Sometimes restraint is updating the file and it's bad formatted,
        that's why we're introduced some sleep before raising an exception.
        """
        old_length: int = -1
        tries: int = 0

        while True:
            xml_content = self.path.read_text(encoding='utf-8')
            try:
                return RestraintJob.create_from_string(xml_content)
            except ET.ParseError as exc:
                if len(xml_content) == old_length:
                    tries = 0
                    old_length = len(xml_content)
                elif tries == len(self.restraint_delays):
                    raise RestraintParsedError(f'Unable to parse {self.path}') from exc
                sleep(self.restraint_delays[tries])
                tries += 1

    def update_info(self) -> None:
        """Update restraint info between two times."""
        if self.new_job:
            self.job = self.new_job
        self.new_job = self.get_restraint_info()

    def get_job_changes(self) -> typing.Optional[RestraintJobDiff]:
        """Get changes in the job."""
        return self.job.diff(self.new_job) if self.new_job else None

    def get_recipe_changes(self, recipe_id: int) -> typing.Optional[RestraintRecipeDiff]:
        """Get changes for a recipe."""
        if job_diff := self.get_job_changes():
            for recipeset_diff in job_diff.recipesets:
                for recipe_diff in recipeset_diff.recipes:
                    if recipe_diff.id == str(recipe_id):
                        return recipe_diff
        return None
