"""Test cases for provisioner interface module."""
import copy
import os
import unittest
from unittest import mock

from plumbing.format import ProvisionData
from plumbing.interface import ProvisionerCore
from plumbing.objects import Host
from plumbing.objects import RecipeSet
from plumbing.objects import ResourceGroup
from tests.const import ASSETS_DIR
from upt.misc import RET


class TestProvisionerCore(unittest.TestCase):
    """Test cases for interface module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        self.bkr = prov_data.get_provisioner('beaker')

    def test_set_reservation_duration(self):
        """Ensure ProvisionerCore's set_reservation_duration can be called."""
        ProvisionerCore.set_reservation_duration(None)

    def test_find_objects_rgs(self):
        """Ensure find_objects works by getting ResourceGroups."""
        # return objects that are instances of ResourceGroup class
        result = self.bkr.find_objects(self.bkr.rgs,
                                       lambda x: x if isinstance(x, ResourceGroup) else None)
        self.assertEqual(self.bkr.rgs, result)

    def test_find_objects_recipests(self):
        """Ensure find_objects works by getting RecipeSets."""
        # return objects that are instances of RecipeSet class
        result = self.bkr.find_objects(self.bkr.rgs,
                                       lambda x: x if isinstance(x, RecipeSet) else None)
        self.assertEqual([self.bkr.rgs[0].recipeset], result)

    def test_find_objects_hosts(self):
        """Ensure find_objects works by getting Hosts."""
        # return objects that are instances of Host class
        result = self.bkr.find_objects(self.bkr.rgs,
                                       lambda x: x if isinstance(x, Host) else None)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts, result)

    def test_get_all_hosts(self):
        """Ensure get_all_hosts works."""
        result = self.bkr.get_all_hosts(self.bkr.rgs)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts, result)

    def test_find_host_object(self):
        """Ensure find_host_object works."""
        first_host = self.bkr.find_host_object(self.bkr.rgs, 123)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts[0], first_host)

        second_host = self.bkr.find_host_object(self.bkr.rgs, 456)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts[1], second_host)

    def test_all_recipes_finished(self):
        """Ensure all_recipes_finished works."""
        result = self.bkr.all_recipes_finished(self.bkr.rgs)
        self.assertFalse(result)

    def test_all_recipes_finished_true(self):
        """Ensure all_recipes_finished works by setting host as 'done processing'."""
        bkr = copy.deepcopy(self.bkr)
        for host in bkr.rgs[0].recipeset.hosts:
            host.done_processing = True

        result = bkr.all_recipes_finished(bkr.rgs)
        self.assertTrue(result)

    @mock.patch('plumbing.end_conditions.time.sleep', lambda x: None)
    @mock.patch('builtins.print', mock.Mock())
    def test_wait_(self):
        """Ensure wait is if-else complete."""
        def one_run(_):
            try:
                _ = one_run.done
                return True
            except AttributeError:
                one_run.done = True
                return False

        mock_cond = mock.Mock()
        mock_cond.evaluate = one_run
        mock_cond.retcode = RET.PROVISIONING_FAILED
        mock_cond._timeout_eval.return_value = True

        mock_release = mock.Mock()
        with mock.patch.object(self.bkr, 'set_reservation_duration') as mock_set:
            self.bkr.rgs[0].wait(mock_cond, mock_set, mock_release)

            mock_set.assert_called()
            mock_release.assert_called()

    @mock.patch('plumbing.end_conditions.time.sleep', lambda x: None)
    @mock.patch('builtins.print', mock.Mock())
    def test_wait_set_reservation_duration_hostname(self):
        """Ensure wait calls set_reservation_duration only on hosts with hostname."""
        mock_cond = mock.Mock()
        mock_cond._timeout_eval.return_value = True
        mock_cond.evaluate.side_effect = iter([False, True, True])

        with mock.patch.object(self.bkr, 'set_reservation_duration') as mock_set:
            resource_group = ResourceGroup()
            resource_group.recipeset.hosts += [Host(), Host({'hostname': 'arg'})]

            resource_group.wait(mock_cond, mock_set, mock.Mock())
            assert mock_set.call_count == 1
