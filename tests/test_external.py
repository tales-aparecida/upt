"""Test cases for external module."""
import os
import unittest
from unittest import mock

from plumbing.format import ProvisionData


class TestExternal(unittest.TestCase):
    """Testcase for External provisioner."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                      'assets/external_req.yaml')
        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.external = self.provision_data.get_provisioner('external')

    @mock.patch.dict(os.environ, {
        'UPT_EXTERNAL_HOST_IP': '10.99.98.97'
    })
    def test_provision(self):
        """Ensure provision works / is called with correct params."""
        with mock.patch.object(self.external, 'provision_host'):
            self.external.provision()

    def test_provision_rg(self):
        """Ensure provision works / is called with only one resource group."""
        self.external.rgs.append('new item')
        with self.assertRaises(Exception):
            self.external.provision()

    def test_provision_hosts(self):
        """Ensure provision works / is called with only one host per recipeset."""
        self.external.rgs[0].recipeset.hosts.append('item two')
        with self.assertRaises(Exception):
            self.external.provision()

    def test_provision_recipe(self):
        """Ensure provision works / is called with only one recipe per recipeset."""
        restraint_xml = """
        <job>
            <recipeSet id="1">
                <recipe id="1" system="hostname1">
                </recipe>
            </recipeSet>
            <recipeSet id="2">
                <recipe id="2">
                </recipe>
            </recipeSet>
        </job>
        """
        self.external.rgs[0].recipeset.restraint_xml = restraint_xml
        with self.assertRaises(Exception):
            self.external.provision()

    def test_provision_noIP(self):
        """Ensure provision with no IP fails"""
        with mock.patch.object(self.external, 'provision_host'):
            with self.assertRaises(KeyError):
                self.external.provision()

    def test_is_provisioned(self):
        """Ensure is_provisioned returns True"""
        is_provisioned = self.external.is_provisioned(self.req_asset[0])
        self.assertTrue(is_provisioned)

    def test_reprovision_aborted(self):
        """Base UPT class, noop by default, should not crash if called"""
        self.external.reprovision_aborted(self.external.rgs[0])

    def test_get_resource_ids(self):
        """Ensure if this method is called exception is raised"""
        with self.assertRaises(Exception):
            self.external.get_resource_ids()

    def test_heartbeat(self):
        """Base UPT class, noop by default, should not crash if called"""
        self.external.heartbeat(self.external.rgs[0], '1234')

    def test_release_rg(self):
        """Base UPT class, noop by default, should not crash if called"""
        self.external.release_rg(self.external.rgs[0])

    def test_update_provisioning_request(self):
        """Base UPT class, noop by default, should not crash if called"""
        self.external.update_provisioning_request(self.external.rgs[0])
