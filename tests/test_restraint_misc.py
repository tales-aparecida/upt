"""Test case from restraint misc module."""
import unittest

from freezegun import freeze_time

from plumbing.objects import Host
from restraint_wrap import misc
from restraint_wrap.task_result import TaskResult
from upt.restraint.file import RestraintTask


class TestRestraintMiscTaskResultFunctions(unittest.TestCase):
    """Test cases for functions related to TaskResult."""

    def setUp(self):
        """Generate setUp."""
        xml_content = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <fetch url="git://"/>
            <params>
              <param name="CKI_MAINTAINERS" value="abc de &lt;abcde@redhat.com&gt;"/>
              <param name="CKI_NAME" value="a3"/>
            </params>
        </task>
        """
        self.restraint_task = RestraintTask.create_from_string(xml_content)
        self.host = Host({'hostname': 'hostname1', 'recipe_id': 1234})

    def test_get_most_recent_task_result(self):
        """Ensure get_most_recent_result works."""
        task_results = [
            TaskResult(self.host, self.restraint_task, 1, 'Pass', 'Completed'),
            TaskResult(self.host, self.restraint_task, 2, 'Running', ''),
            TaskResult(self.host, self.restraint_task, 2, 'FAIL', 'Completed'),
            TaskResult(self.host, self.restraint_task, 4, 'FAIL', 'Completed')]

        result = misc.get_most_recent_task_result(self.host.recipe_id, 2, task_results)
        self.assertEqual(task_results[2], result)

        result = misc.get_most_recent_task_result(self.host.recipe_id, 333, task_results)
        self.assertIsNone(result)

    def test_get_first_start_time_with_a_previous_date_in_task_results(self):
        """Ensure get_first_time works when we have a previous date in task_results."""
        expected = '2022-02-28T18:24:06+0000'
        task_results = [
            TaskResult(self.host, self.restraint_task, 1, 'Pass', 'Completed'),
            TaskResult(self.host, self.restraint_task, 2, 'FAIL', 'Completed', expected),
            TaskResult(self.host, self.restraint_task, 2, 'Running', ''),
            TaskResult(self.host, self.restraint_task, 2, 'WARN', 'Completed',
                       '2022-02-28T18:25:06+0000'),
            TaskResult(self.host, self.restraint_task, 4, 'FAIL', 'Completed')]

        result = misc.get_first_start_time(self.host.recipe_id, 2, task_results)
        self.assertEqual(expected, result)

    @freeze_time('2020-11-13T16:34:45.912589Z', tz_offset=1)
    def test_get_first_start_time_without_previous_date_in_task_results(self):
        """Ensure get_first_time works when we don't have a previous date in task_results."""
        expected = '2020-11-13T16:34:45.912589+00:00'
        task_results = [
            TaskResult(self.host, self.restraint_task, 1, 'Pass', 'Completed'),
            TaskResult(self.host, self.restraint_task, 2, 'FAIL', 'Completed', expected),
            TaskResult(self.host, self.restraint_task, 2, 'Running', ''),
            TaskResult(self.host, self.restraint_task, 2, 'WARN', 'Completed',
                       '2022-02-28T18:25:06+0000'),
            TaskResult(self.host, self.restraint_task, 4, 'FAIL', 'Completed')]

        result = misc.get_first_start_time(self.host.recipe_id, 5, task_results)
        self.assertEqual(expected, result)

        result = misc.get_first_start_time(self.host.recipe_id, 10, [])
        self.assertEqual(expected, result)
