"""Test cases for misc module."""
from importlib.resources import files
from pathlib import Path
import shutil
import tempfile
import unittest

from upt.restraint.file import RestraintJobDiff
from upt.restraint.file import RestraintJobWatcher
from upt.restraint.file import RestraintParsedError
from upt.restraint.file import RestraintRecipeDiff

ASSETS = files(__package__) / 'assets/restraint_file'


class TestRestraintJobWatcher(unittest.TestCase):
    """Test restraint watcher."""

    def setUp(self):
        """Setup."""
        initial_file = Path(ASSETS, 'job_watcher.xml')
        self.tmp_dir = tempfile.mkdtemp()
        self.watcher_file = Path(self.tmp_dir, 'job.xml')
        shutil.copy(initial_file, self.watcher_file)
        self.watcher = RestraintJobWatcher(self.watcher_file)

    def tearDown(self):
        """Clean up."""
        shutil.rmtree(self.tmp_dir)

    def update_restraint_file(self, new_path):
        """Update file with new status."""
        updated_file = Path(ASSETS, new_path)
        shutil.copy(updated_file, self.watcher_file)

    def test_without_any_change(self):
        """Test without any update."""
        self.assertIsNone(self.watcher.get_job_changes())
        self.assertIsNone(self.watcher.get_recipe_changes(1))

    def test_with_changes(self):
        """Test watcher with one update."""
        self.update_restraint_file('job_watcher_update.xml')
        self.watcher.update_info()
        self.assertIsNone(self.watcher.get_recipe_changes(999))
        self.assertIsInstance(self.watcher.get_job_changes(), RestraintJobDiff)
        self.assertIsInstance(self.watcher.get_recipe_changes(1), RestraintRecipeDiff)

    def test_with_bad_xml(self):
        """Test with a ban xml file."""
        # We don't want retries, we looking for the fail
        self.watcher.restraint_delays = [0., 0.1]
        self.update_restraint_file('job_watcher_broken.xml')
        with self.assertRaises(RestraintParsedError):
            self.watcher.update_info()
