"""Test cases for actions module."""
import unittest
from unittest import mock

from restraint_wrap.actions import ActionOnResult


class TestActions(unittest.TestCase):
    """Ensure actions ActionOnResult works as expected."""

    def setUp(self) -> None:
        self.nop = ActionOnResult('ERROR', [], 'Message string', a={1}, b={2})
        self.nop2 = ActionOnResult('ERROR', [], 'Message string', a={1}, b={2, 3})
        self.empty = ActionOnResult('ERROR', [], 'Message string')

    def test_cond_eval_tuple(self):
        """Ensure .eval() returns True when conditions match at least one tuple member."""
        result = self.nop2.eval(a=1, b=2)
        self.assertTrue(result)
        result = self.nop2.eval(a=1, b=3)
        self.assertTrue(result)
        result = self.nop2.eval(a=1, b=5)
        self.assertFalse(result)

    def test_cond_eval_exact(self):
        """Ensure .eval() returns True when conditions match exactly."""
        result = self.nop.eval(a=1, b=2)
        self.assertTrue(result)

    def test_cond_eval_fail(self):
        """Ensure .eval() returns None when conditions don't match at all."""
        result = self.nop.eval(a=1000, b=2000)
        self.assertIsNone(result)

    @mock.patch('upt.logger.LOGGER.error', mock.Mock())
    def test_cond_eval_mismatch(self):
        """Ensure .eval() raises exception when testing for something unknown."""
        with self.assertRaises(RuntimeError):
            self.nop.eval(f=5)

    def test_cond_eval_no_params(self):
        """Ensure .eval() returns None when no conditions are set in action object."""
        self.assertIsNone(self.empty.eval())

    def test_str_method(self):
        """Ensure __str__ shows conditions set."""
        self.assertEqual(str(self.nop), 'a={1} b={2}')
