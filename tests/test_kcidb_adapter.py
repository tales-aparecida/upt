"""Test cases for kcidb_adapter module."""
from datetime import timedelta
import os
import unittest
from unittest import mock

from cki_lib.kcidb.file import KCIDBFile
from dateutil.parser import parse as date_parse
from freezegun import freeze_time

from plumbing.objects import Host
from restraint_wrap.kcidb_adapter import KCIDBTestAdapter
from restraint_wrap.kcidb_adapter import RestraintStandaloneTest
from restraint_wrap.task_result import TaskResult
from tests.utils import KCIDB_DEFAULT_JSON
from tests.utils import create_temporary_kcidb
from upt.misc import OutputDirCounter
from upt.restraint.file import RestraintRecipe

DEF_ENV_MOCK_DICT = {'CI_JOB_ID': '1234', 'CI_PIPELINE_ID': '5678', 'CI_PROJECT_PATH': 'path',
                     'CI_PROJECT_ID': '40', 'CI_SERVER_URL': 'https://', 'CI_JOB_STAGE': 'test',
                     'CI_JOB_URL': 'https://gitlab/job/1234', 'BEAKER_URL': 'https://beaker',
                     'CI_JOB_NAME': 'test x86_64', 'job_created_at': '2019-10-24T13:40:46.632298Z',
                     'CI_COMMIT_SHA': 'deadbeef',
                     'CI_PROJECT_DIR': 'whatever',
                     'GITLAB_CI': 'true',
                     'KCIDB_DUMPFILE_NAME': 'kcidb_all.json',
                     'CI_COMMIT_REF_NAME': 'block',
                     'REVISION_PATH': 'whatever-revision-path',
                     'KCIDB_CHECKOUT_ID': 'redhat:5678',
                     'KCIDB_BUILD_ID': 'redhat:1234'}


class TestKCIDBTestAdapter(unittest.TestCase):
    """Test cases for KCIDBTestAdapter class."""

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_artifacts_path(self):
        """Ensure artifacts_path works."""
        with create_temporary_kcidb() as kcidb_path:
            kcidb_data = KCIDBFile(kcidb_path)
        adapter = KCIDBTestAdapter(
            **{'output': 'whatever-dir', 'kcidb_data': kcidb_data, 'upload': False,
               'instance_no': 1})

        expected = '1234/redhat:5678/build_x86_64_redhat:1234/tests/'
        self.assertEqual(expected, adapter.artifacts_path)


class TestRestraintStandaloneTest(unittest.TestCase):
    """Test cases for RestraintStandaloneTest class."""

    # pylint: disable=too-many-instance-attributes
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @freeze_time('2020-11-13T16:34:45.912589Z', tz_offset=1)
    def setUp(self) -> None:
        recipe_xml = (
            '<recipe>'
            '  <tasks>'
            '    <task id="1" name="a3" status="Completed" result="PASS">'
            '      <fetch url="git://"/>'
            '      <params>'
            '        <param name="CKI_ID" value="1"/>'
            '        <param name="CKI_NAME" value="a3"/>'
            '      </params>'
            '    </task>'
            '    <task id="2" name="a4" status="Completed">'
            '      <fetch url="git://"/>'
            '      <params>'
            '        <param name="CKI_ID" value="2"/>'
            '        <param name="CKI_UNIVERSAL_ID" value="some/path"/>'
            '      </params>'
            '    </task>'
            '  </tasks>'
            '</recipe>'
        )
        self.restraint_tasks = RestraintRecipe.create_from_string(recipe_xml).tasks
        self.host = Host({'hostname': 'hostname1', 'recipe_id': 1234, 'done_processing': True})
        self.host.counter = OutputDirCounter()
        self.host_no_retcode = Host({'hostname': 'hostname1', 'recipe_id': 1234})
        self.host_no_retcode.counter = OutputDirCounter()
        self.task_result = TaskResult(self.host, self.restraint_tasks[0], 1, 'PASS', 'Completed')
        self.task_result.kcidb_status = 'PASS'
        self.mock_adapter = mock.Mock()
        self.mock_adapter.checkout_id = '92b8f402aa964f209772e30190af5de818af996c'
        with create_temporary_kcidb() as kcidb_path:
            self.mock_adapter.kcidb_data = KCIDBFile(kcidb_path)
        self.mock_adapter.job_id = '1234'
        self.mock_adapter.build = KCIDB_DEFAULT_JSON['builds'][0]
        self.mock_adapter.tests = KCIDB_DEFAULT_JSON['tests']
        self.mock_adapter.upload = False
        self.mock_adapter.instance_no = 1

        self.standalone_test = RestraintStandaloneTest(self.mock_adapter, self.task_result,
                                                       run_suffix='job.01', testplan=False)
        task_result_no_retcode = TaskResult(self.host_no_retcode,
                                            self.restraint_tasks[0], 1, 'PASS', 'Completed')
        task_result_no_retcode.kcidb_status = 'PASS'
        self.standalone_test_no_retcode = RestraintStandaloneTest(self.mock_adapter,
                                                                  task_result_no_retcode,
                                                                  run_suffix='job.01',
                                                                  testplan=False)

        task_result_normalize = TaskResult(self.host, self.restraint_tasks[0],
                                           1, 'WARN', 'Completed')
        task_result_normalize.kcidb_status = 'ERROR'
        self.standalone_test_normalize = RestraintStandaloneTest(self.mock_adapter,
                                                                 task_result_normalize,
                                                                 run_suffix='job.01',
                                                                 testplan=False)

    @mock.patch('restraint_wrap.kcidb_adapter.upload_file')
    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_output_files_uploading_files(self, mock_upload_file):
        """Ensure RestraintStandaloneTest output_files works when uploading files."""
        output_path = 'run/run.done'
        job_suffix = 'job.01'
        path = f'{output_path}/recipes/1234/tasks/1'
        full_path = f's3://my-dst-bucket/{path}'
        mock_adapter = self.mock_adapter
        mock_adapter.build_id = 'redhat:1'
        mock_adapter.upload = True
        mock_adapter.output = output_path
        mock_adapter.artifacts_path = 'something'

        mock_upload_file.side_effect = [f'{full_path}/file1', f'{full_path}/file2']

        with mock.patch.object(self.task_result, 'output_files_from_path',
                               lambda *args: [{'name': 'file1', 'path': path},
                                              {'name': 'file2', 'path': path}]):
            ret_obj = RestraintStandaloneTest(mock_adapter, self.task_result,
                                              run_suffix=job_suffix, testplan=True)
            # If test plan we don't get any files
            self.assertListEqual(
                ret_obj.output_files,
                []
            )
            # If no test plan, we get all the files
            ret_obj.testplan = False
            self.assertListEqual(
                ret_obj.output_files,
                [
                    {'name': 'file1', 'url': f'{full_path}/file1'},
                    {'name': 'file2', 'url': f'{full_path}/file2'},
                ]
            )

    @mock.patch('restraint_wrap.kcidb_adapter.convert_path_to_link')
    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_output_files_wihout_uploading_files(self, mock_convert_link):
        """Ensure RestraintStandaloneTest output_files works when not uploading files."""
        output_path = 'run/run.done'
        job_suffix = 'job.01'
        path = f'{output_path}/recipes/1234/tasks/1'
        full_path = f's3://mybucket/{path}'
        mock_adapter = self.mock_adapter
        mock_adapter.build_id = 'redhat:1'
        mock_adapter.output = output_path
        mock_adapter.artifacts_path = 'something'

        mock_convert_link.side_effect = [f'{full_path}/file1', f'{full_path}/file2']

        with mock.patch.object(self.task_result, 'output_files_from_path',
                               lambda *args: [{'name': 'file1', 'path': path},
                                              {'name': 'file2', 'path': path}]):
            ret_obj = RestraintStandaloneTest(mock_adapter, self.task_result,
                                              run_suffix=job_suffix, testplan=True)
            # If test plan we don't get any files
            self.assertListEqual(
                ret_obj.output_files,
                []
            )
            # If no test plan, we get all the files
            ret_obj.testplan = False
            self.assertListEqual(
                ret_obj.output_files,
                [
                    {'name': 'file1', 'url': f'{full_path}/file1'},
                    {'name': 'file2', 'url': f'{full_path}/file2'},
                ]
            )

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_duration(self):
        """Ensure _duration and finish_time work."""
        mock_start_time = "2020-11-09T18:06:48.872084Z"
        task_result = TaskResult(self.host, self.restraint_tasks[0], 1, 'PASS', 'Completed')
        delta = 5
        with mock.patch.object(task_result, 'start_time', mock_start_time):
            time_frozen_at = (date_parse(mock_start_time) + timedelta(seconds=delta)).isoformat()
            with freeze_time(time_frozen_at, tz_offset=1):
                standalone_test = RestraintStandaloneTest(self.mock_adapter, task_result, 'job.01')
                self.assertEqual(standalone_test.duration, delta)
                self.assertEqual(standalone_test.finish_time, '2020-11-09T18:06:53.872084+00:00')

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_render(self):
        """Ensure render works."""
        self.mock_adapter.output = 'run/run.done'

        with mock.patch.object(self.standalone_test, 'testplan', True):
            result = self.standalone_test.render()
            self.assertEqual(result['status'], 'PASS')
            self.assertEqual(result['start_time'], self.standalone_test.start_time)

        with mock.patch.object(self.standalone_test, 'testplan', False):
            result = self.standalone_test.render()
            self.assertEqual(result['misc']['fetch_url'], 'git://')
            self.assertEqual(result['environment'], {'comment': 'hostname1'})
            self.assertNotEqual(result['misc']['provenance'], [])

        with mock.patch.dict(os.environ, {'GITLAB_CI': 'false', 'BEAKER_URL': ''}):
            result = self.standalone_test.render()
            self.assertEqual(result['misc']['provenance'], [])

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_standalone_test_finish_time(self):
        """Ensure finish_time is set to beaker key."""
        task_result = TaskResult(self.host, self.restraint_tasks[1], 1, None, 'Completed')
        standalone_test = RestraintStandaloneTest(self.mock_adapter, task_result,
                                                  run_suffix='job.01', testplan=False)
        standalone_test.finish_time = 'some'

        self.assertEqual('some', standalone_test.misc['beaker']['finish_time'])

    @mock.patch('restraint_wrap.kcidb_adapter.RestraintStandaloneTest.output_files',
                new_callable=mock.PropertyMock)
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_render_path(self, mock_files):
        """Ensure render sets path from univesal_id."""
        task_result = TaskResult(self.host, self.restraint_tasks[1], 1, None, 'Completed')
        task_result.kcidb_status = 'PASS'
        standalone_test = RestraintStandaloneTest(self.mock_adapter, task_result,
                                                  run_suffix='job.01', testplan=False)
        self.assertEqual('some.path', standalone_test.render()['path'])
