"""A result of a task run, with extra arguments to simplify working with it."""
import os

from restraint_wrap.misc import get_first_start_time
from upt.misc import is_task_waived


class TaskResult:
    # pylint: disable=too-many-instance-attributes,too-few-public-methods
    """A result of task that was run."""

    def __init__(
        self,
        host,
        restraint_task,
        task_id,
        result,
        status,
        start_time=None,
        lwd_hit=False,
        ewd_hit=False,
    ):
        # pylint: disable=too-many-arguments
        """Create the object."""
        # The task result is identified by recipe_id
        self.recipe_id = int(host.recipe_id)
        self.task = restraint_task
        self.task_id = int(task_id)
        self.prefix_recipe_id_path = f'recipes/{self.recipe_id}'
        if not start_time:
            self.start_time = get_first_start_time(self.recipe_id, self.task_id, host.task_results)
        else:
            self.start_time = start_time
        # Result & status fields from restraint.
        self.result = result
        self.status = status
        # Aggregated result, called "status" in kcidb. Set by protocol runner on each action.
        self.kcidb_status = None
        self.host = host
        self.statusresult = status if not result else f'{status}: {result}'

        # Derived
        self.waived = is_task_waived(restraint_task)
        self.testname = restraint_task.name

        self.fetch_url = restraint_task.fetch.url if restraint_task.fetch else None

        self.test_maintainers = []
        # Comes from kpet-db.
        for maint in restraint_task.maintainers:
            self.test_maintainers.append(
                {
                    'name': maint.name,
                    'email': maint.email,
                    'gitlab': maint.gitlab
                })

        # comes from kpet-db
        self.universal_id = restraint_task.get_param_value_by_name('CKI_UNIVERSAL_ID')
        self.cki_name = restraint_task.get_param_value_by_name('CKI_NAME')
        self.cki_id = restraint_task.get_param_value_by_name('CKI_ID')

        self.subtask_results = []
        self.lwd_hit = lwd_hit
        self.ewd_hit = ewd_hit

    @property
    def is_cki_test(self):
        """Return True if the test has cki_name and universal_id."""
        return self.universal_id is not None and self.cki_name is not None

    @property
    def is_boot_task(self):
        """Check if the task is a boot task."""
        return self.universal_id == 'boot' or self.testname.lower() == 'boot test'

    def output_files_from_path(self, path_prefix):
        """Return a list of files for this recipe_id:task_id in path_prefix directory."""
        path2task = os.path.join(path_prefix, f'{self.prefix_recipe_id_path}/tasks/{self.task_id}')

        all_files = []
        for dirpath, _, files in os.walk(path2task):
            all_files += [{'name': f, 'path': dirpath} for f in files]

        return all_files
